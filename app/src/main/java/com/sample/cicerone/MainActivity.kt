package com.sample.cicerone

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.android.SupportFragmentNavigator

class MainActivity : AppCompatActivity() {

    private val cicerone = Cicerone.create()

    private val navigator = object : SupportFragmentNavigator(supportFragmentManager, R.id.fragment_container) {
        override fun exit() {
            finish()
        }

        override fun showSystemMessage(message: String?) {
        }

        override fun createFragment(screenKey: String, data: Any?): Fragment {
            when (screenKey) {
                MainFragment.SCREEN_TAG -> {
                    return MainFragment()
                }
                AuthFragment.SCREEN_TAG -> return AuthFragment()
            }
            throw IllegalArgumentException("Unknown screen key: $screenKey")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            cicerone.router.newRootScreen(MainFragment.SCREEN_TAG)
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        cicerone.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        cicerone.navigatorHolder.removeNavigator()
    }

    fun getRouter() = cicerone.router
}
