package com.sample.cicerone

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class AuthFragment: Fragment() {
    companion object {
        const val SCREEN_TAG = "auth"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_layout, container, false)
        view.findViewById<TextView>(R.id.text).text = "AAAAAAAAAAA"
        view.findViewById<View>(R.id.root_view).setBackgroundColor(resources.getColor(R.color.colorPrimary))
        return view
    }
}
