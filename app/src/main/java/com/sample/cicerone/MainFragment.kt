package com.sample.cicerone

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class MainFragment: Fragment() {
    companion object {
        const val SCREEN_TAG = "main"
    }

    private val needAuth = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_layout, container, false)
        view.findViewById<TextView>(R.id.text).text = "MMMMMMMMMMMMMMMMMMM"
        view.findViewById<View>(R.id.root_view).setBackgroundColor(resources.getColor(R.color.colorAccent))
        return view
    }

    override fun onResume() {
        super.onResume()
        if (needAuth) {
            (activity as MainActivity).getRouter()?.navigateTo(AuthFragment.SCREEN_TAG)
        }
    }
}
